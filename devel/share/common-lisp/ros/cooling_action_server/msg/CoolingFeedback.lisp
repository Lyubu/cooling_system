; Auto-generated. Do not edit!


(cl:in-package cooling_action_server-msg)


;//! \htmlinclude CoolingFeedback.msg.html

(cl:defclass <CoolingFeedback> (roslisp-msg-protocol:ros-message)
  ((RPM_measure
    :reader RPM_measure
    :initarg :RPM_measure
    :type cl:float
    :initform 0.0))
)

(cl:defclass CoolingFeedback (<CoolingFeedback>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <CoolingFeedback>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'CoolingFeedback)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name cooling_action_server-msg:<CoolingFeedback> is deprecated: use cooling_action_server-msg:CoolingFeedback instead.")))

(cl:ensure-generic-function 'RPM_measure-val :lambda-list '(m))
(cl:defmethod RPM_measure-val ((m <CoolingFeedback>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader cooling_action_server-msg:RPM_measure-val is deprecated.  Use cooling_action_server-msg:RPM_measure instead.")
  (RPM_measure m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <CoolingFeedback>) ostream)
  "Serializes a message object of type '<CoolingFeedback>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'RPM_measure))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <CoolingFeedback>) istream)
  "Deserializes a message object of type '<CoolingFeedback>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'RPM_measure) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<CoolingFeedback>)))
  "Returns string type for a message object of type '<CoolingFeedback>"
  "cooling_action_server/CoolingFeedback")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'CoolingFeedback)))
  "Returns string type for a message object of type 'CoolingFeedback"
  "cooling_action_server/CoolingFeedback")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<CoolingFeedback>)))
  "Returns md5sum for a message object of type '<CoolingFeedback>"
  "13d77859e8628400650b2df5acbd0227")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'CoolingFeedback)))
  "Returns md5sum for a message object of type 'CoolingFeedback"
  "13d77859e8628400650b2df5acbd0227")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<CoolingFeedback>)))
  "Returns full string definition for message of type '<CoolingFeedback>"
  (cl:format cl:nil "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======~%#feedback~%float32 RPM_measure~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'CoolingFeedback)))
  "Returns full string definition for message of type 'CoolingFeedback"
  (cl:format cl:nil "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======~%#feedback~%float32 RPM_measure~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <CoolingFeedback>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <CoolingFeedback>))
  "Converts a ROS message object to a list"
  (cl:list 'CoolingFeedback
    (cl:cons ':RPM_measure (RPM_measure msg))
))
