
(cl:in-package :asdf)

(defsystem "cooling_action_server-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :actionlib_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "CoolingAction" :depends-on ("_package_CoolingAction"))
    (:file "_package_CoolingAction" :depends-on ("_package"))
    (:file "CoolingActionFeedback" :depends-on ("_package_CoolingActionFeedback"))
    (:file "_package_CoolingActionFeedback" :depends-on ("_package"))
    (:file "CoolingActionGoal" :depends-on ("_package_CoolingActionGoal"))
    (:file "_package_CoolingActionGoal" :depends-on ("_package"))
    (:file "CoolingActionResult" :depends-on ("_package_CoolingActionResult"))
    (:file "_package_CoolingActionResult" :depends-on ("_package"))
    (:file "CoolingFeedback" :depends-on ("_package_CoolingFeedback"))
    (:file "_package_CoolingFeedback" :depends-on ("_package"))
    (:file "CoolingGoal" :depends-on ("_package_CoolingGoal"))
    (:file "_package_CoolingGoal" :depends-on ("_package"))
    (:file "CoolingResult" :depends-on ("_package_CoolingResult"))
    (:file "_package_CoolingResult" :depends-on ("_package"))
  ))