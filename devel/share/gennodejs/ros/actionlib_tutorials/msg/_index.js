
"use strict";

let CoolingActionResult = require('./CoolingActionResult.js');
let CoolingActionFeedback = require('./CoolingActionFeedback.js');
let CoolingResult = require('./CoolingResult.js');
let CoolingAction = require('./CoolingAction.js');
let CoolingFeedback = require('./CoolingFeedback.js');
let CoolingGoal = require('./CoolingGoal.js');
let CoolingActionGoal = require('./CoolingActionGoal.js');

module.exports = {
  CoolingActionResult: CoolingActionResult,
  CoolingActionFeedback: CoolingActionFeedback,
  CoolingResult: CoolingResult,
  CoolingAction: CoolingAction,
  CoolingFeedback: CoolingFeedback,
  CoolingGoal: CoolingGoal,
  CoolingActionGoal: CoolingActionGoal,
};
