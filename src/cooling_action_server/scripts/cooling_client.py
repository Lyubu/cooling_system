#! /usr/bin/env python

from __future__ import print_function
from fan_control import getCpuTemperature
import rospy
import actionlib
import cooling_action_server.msg

RPM_Max = 1500
RPM_Min = 0

def rpm_set()
        temp = getCpuTemperature()
        if temp < MIN_TEMP:
            rpm = RPM_Min
        elif temp > MAX_TEMP:
            rpm = RPM_Max
        else:
            step = (RPM_Max - RPM_Min)/(MAX_TEMP - MIN_TEMP)   
            temp -= MIN_TEMP
            rpm = RPM_MIN + ( round(temp) * step ))
        return ()

def cooling_client():
    rpm_set()

    # Creates the SimpleActionClient, passing the type of the action
    # (coolingAction) to the constructor.
    client = actionlib.SimpleActionClient('cooling', cooling_action_server.msg.coolingAction)

    # Waits until the action server has started up and started
    # listening for goals.
    client.wait_for_server()

    # Creates a goal to send to the action server.
    goal = cooling_action_server.msg.coolingGoal(rpm_set = rpm)

    # Sends the goal to the action server.
    client.send_goal(goal)

    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result()  # A coolingResult

if __name__ == '__main__':
    try:
        # Initializes a rospy node so that the SimpleActionClient can
        # publish and subscribe over ROS.
        rospy.init_node('cooling_client_py')
        result = cooling_client()
        # print("Result: End")
    except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr)
