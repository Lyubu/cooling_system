#!/usr/bin/python
# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
import time
import signal
import sys
import os

# Configuration
FAN_PIN = 18            # BCM pin used to drive PWM fan
WAIT_TIME = 30           # [s] Time to wait between each refresh
PWM_FREQ = 25000        # [Hz] 25kHz for PWM control

# Configurable temperature and fan speed
MIN_TEMP = 35
MAX_TEMP = 50
FAN_LOW = 40
FAN_HIGH = 100
FAN_OFF = 0
FAN_MAX = 100

# Get CPU's temperature
def getCpuTemperature():
    tfile = open('/sys/bus/w1/devices/28-00000a29259e/w1_slave')
    text = tfile.read()
    tfile.close()
    # Use the line break to split the string into an array and take the second line
    secondline = text.split('\n')[1]
    # Use the space to split the string into an array and take the last one
    temperaturedata = secondline.split(' ')[9]
    # Take the value behind 't = ' and convert to floating point
    temperature = float(temperaturedata[2:])
    # Convert units to degrees Celsius
    temp = temperature / 1000
    return temp

# Set fan speed
def setFanSpeed(speed):
    fan.start(speed)
    return()

# Handle fan speed
def handleFanSpeed():
    temp = getCpuTemperature()
    current_time = datetime.datetime.now()
    print('CPU temperature: '+ str(temp) + ' @ ' + str(current_time))
    # Turn off the fan if temperature is below MIN_TEMP
    if temp < MIN_TEMP:
        setFanSpeed(FAN_OFF)
        print("Fan OFF") 
    # Set fan speed to MAXIMUM if the temperature is above MAX_TEMP
    elif temp > MAX_TEMP:
        setFanSpeed(FAN_MAX)
        print("Fan MAX") 
    # Caculate dynamic fan speed
    else:
        step = (FAN_HIGH - FAN_LOW)/(MAX_TEMP - MIN_TEMP)   
        temp -= MIN_TEMP
        setFanSpeed(FAN_LOW + ( round(temp) * step ))
        print(str(FAN_LOW + ( round(temp) * step )) + "%") 
    return ()

try:
    # Setup GPIO pin
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(FAN_PIN, GPIO.OUT, initial=GPIO.LOW)
    fan = GPIO.PWM(FAN_PIN,PWM_FREQ)
    setFanSpeed(FAN_OFF)
    # Handle fan speed every WAIT_TIME sec
    while True:
        handleFanSpeed()
        time.sleep(WAIT_TIME)

except KeyboardInterrupt: # trap a CTRL+C keyboard interrupt
    setFanSpeed(FAN_HIGH)
    #GPIO.cleanup() # resets all GPIO ports used by this function
