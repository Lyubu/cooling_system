#! /usr/bin/env python

from read_fan_speed import rpm_measure
import rospy
import actionlib
import cooling_action_server.msg


class CoolingAction(object):
    # create messages that are used to publish feedback/result
    _feedback = cooling_action_server.msg.CoolingFeedback()
    _result   = cooling_action_server.msg.CoolingResult()
    
    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.CoolingActionServer(self._action_name, cooling_action_server.msg.CoolingAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()
      
    def execute_cb(self, goal):
        # helper variables
        r = rospy.Rate(1)
        success = True
        
        
        # publish info to the console for the user
        rospy.loginfo('%s: Required RPM: %i, Actual RPM: %i' % (self._action_name, goal.rpm_set, self._feedback)
        
        # start executing the action   
        # check that preempt has not been requested by the client
        if self._as.is_preempt_requested():
            rospy.loginfo('%s: Preempted' % self._action_name)
            self._as.set_preempted()
            success = False
            break
        self._feedback.rpm_measure = rpm_measure(n)
        # publish the feedback
        self._as.publish_feedback(self._feedback)
        r.sleep()
          
        if success:
            self._result = self._feedback
            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(self._result)
        
if __name__ == '__main__':
    rospy.init_node('cooling')
    server = CoolingAction(rospy.get_name())
    rospy.spin()
