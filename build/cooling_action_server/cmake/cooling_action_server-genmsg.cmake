# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "cooling_action_server: 7 messages, 0 services")

set(MSG_I_FLAGS "-Icooling_action_server:/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg;-Iactionlib_msgs:/opt/ros/melodic/share/actionlib_msgs/cmake/../msg;-Istd_msgs:/opt/ros/melodic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(cooling_action_server_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg" NAME_WE)
add_custom_target(_cooling_action_server_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "cooling_action_server" "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg" ""
)

get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg" NAME_WE)
add_custom_target(_cooling_action_server_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "cooling_action_server" "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg" "actionlib_msgs/GoalID:cooling_action_server/CoolingGoal:std_msgs/Header"
)

get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg" NAME_WE)
add_custom_target(_cooling_action_server_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "cooling_action_server" "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg" ""
)

get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg" NAME_WE)
add_custom_target(_cooling_action_server_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "cooling_action_server" "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg" "actionlib_msgs/GoalID:actionlib_msgs/GoalStatus:cooling_action_server/CoolingFeedback:std_msgs/Header"
)

get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingAction.msg" NAME_WE)
add_custom_target(_cooling_action_server_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "cooling_action_server" "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingAction.msg" "actionlib_msgs/GoalID:actionlib_msgs/GoalStatus:cooling_action_server/CoolingActionResult:cooling_action_server/CoolingActionFeedback:cooling_action_server/CoolingResult:cooling_action_server/CoolingActionGoal:cooling_action_server/CoolingGoal:std_msgs/Header:cooling_action_server/CoolingFeedback"
)

get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg" NAME_WE)
add_custom_target(_cooling_action_server_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "cooling_action_server" "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg" ""
)

get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg" NAME_WE)
add_custom_target(_cooling_action_server_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "cooling_action_server" "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg" "actionlib_msgs/GoalID:actionlib_msgs/GoalStatus:cooling_action_server/CoolingResult:std_msgs/Header"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/cooling_action_server
)
_generate_msg_cpp(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/cooling_action_server
)
_generate_msg_cpp(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/cooling_action_server
)
_generate_msg_cpp(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/cooling_action_server
)
_generate_msg_cpp(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/cooling_action_server
)
_generate_msg_cpp(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/cooling_action_server
)
_generate_msg_cpp(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/cooling_action_server
)

### Generating Services

### Generating Module File
_generate_module_cpp(cooling_action_server
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/cooling_action_server
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(cooling_action_server_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(cooling_action_server_generate_messages cooling_action_server_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_cpp _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_cpp _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_cpp _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_cpp _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingAction.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_cpp _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_cpp _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_cpp _cooling_action_server_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(cooling_action_server_gencpp)
add_dependencies(cooling_action_server_gencpp cooling_action_server_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS cooling_action_server_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/cooling_action_server
)
_generate_msg_eus(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/cooling_action_server
)
_generate_msg_eus(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/cooling_action_server
)
_generate_msg_eus(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/cooling_action_server
)
_generate_msg_eus(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/cooling_action_server
)
_generate_msg_eus(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/cooling_action_server
)
_generate_msg_eus(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/cooling_action_server
)

### Generating Services

### Generating Module File
_generate_module_eus(cooling_action_server
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/cooling_action_server
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(cooling_action_server_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(cooling_action_server_generate_messages cooling_action_server_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_eus _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_eus _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_eus _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_eus _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingAction.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_eus _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_eus _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_eus _cooling_action_server_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(cooling_action_server_geneus)
add_dependencies(cooling_action_server_geneus cooling_action_server_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS cooling_action_server_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/cooling_action_server
)
_generate_msg_lisp(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/cooling_action_server
)
_generate_msg_lisp(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/cooling_action_server
)
_generate_msg_lisp(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/cooling_action_server
)
_generate_msg_lisp(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/cooling_action_server
)
_generate_msg_lisp(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/cooling_action_server
)
_generate_msg_lisp(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/cooling_action_server
)

### Generating Services

### Generating Module File
_generate_module_lisp(cooling_action_server
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/cooling_action_server
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(cooling_action_server_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(cooling_action_server_generate_messages cooling_action_server_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_lisp _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_lisp _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_lisp _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_lisp _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingAction.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_lisp _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_lisp _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_lisp _cooling_action_server_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(cooling_action_server_genlisp)
add_dependencies(cooling_action_server_genlisp cooling_action_server_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS cooling_action_server_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/cooling_action_server
)
_generate_msg_nodejs(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/cooling_action_server
)
_generate_msg_nodejs(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/cooling_action_server
)
_generate_msg_nodejs(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/cooling_action_server
)
_generate_msg_nodejs(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/cooling_action_server
)
_generate_msg_nodejs(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/cooling_action_server
)
_generate_msg_nodejs(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/cooling_action_server
)

### Generating Services

### Generating Module File
_generate_module_nodejs(cooling_action_server
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/cooling_action_server
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(cooling_action_server_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(cooling_action_server_generate_messages cooling_action_server_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_nodejs _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_nodejs _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_nodejs _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_nodejs _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingAction.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_nodejs _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_nodejs _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_nodejs _cooling_action_server_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(cooling_action_server_gennodejs)
add_dependencies(cooling_action_server_gennodejs cooling_action_server_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS cooling_action_server_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/cooling_action_server
)
_generate_msg_py(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/cooling_action_server
)
_generate_msg_py(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/cooling_action_server
)
_generate_msg_py(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/cooling_action_server
)
_generate_msg_py(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/cooling_action_server
)
_generate_msg_py(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/cooling_action_server
)
_generate_msg_py(cooling_action_server
  "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/melodic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/cooling_action_server
)

### Generating Services

### Generating Module File
_generate_module_py(cooling_action_server
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/cooling_action_server
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(cooling_action_server_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(cooling_action_server_generate_messages cooling_action_server_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingGoal.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_py _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionGoal.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_py _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingFeedback.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_py _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionFeedback.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_py _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingAction.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_py _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingResult.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_py _cooling_action_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/jimmy/cooling_ws/devel/share/cooling_action_server/msg/CoolingActionResult.msg" NAME_WE)
add_dependencies(cooling_action_server_generate_messages_py _cooling_action_server_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(cooling_action_server_genpy)
add_dependencies(cooling_action_server_genpy cooling_action_server_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS cooling_action_server_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/cooling_action_server)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/cooling_action_server
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_cpp)
  add_dependencies(cooling_action_server_generate_messages_cpp actionlib_msgs_generate_messages_cpp)
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(cooling_action_server_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/cooling_action_server)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/cooling_action_server
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_eus)
  add_dependencies(cooling_action_server_generate_messages_eus actionlib_msgs_generate_messages_eus)
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(cooling_action_server_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/cooling_action_server)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/cooling_action_server
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_lisp)
  add_dependencies(cooling_action_server_generate_messages_lisp actionlib_msgs_generate_messages_lisp)
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(cooling_action_server_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/cooling_action_server)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/cooling_action_server
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_nodejs)
  add_dependencies(cooling_action_server_generate_messages_nodejs actionlib_msgs_generate_messages_nodejs)
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(cooling_action_server_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/cooling_action_server)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/cooling_action_server\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/cooling_action_server
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_py)
  add_dependencies(cooling_action_server_generate_messages_py actionlib_msgs_generate_messages_py)
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(cooling_action_server_generate_messages_py std_msgs_generate_messages_py)
endif()
